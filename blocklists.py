#!/usr/bin/env python3

import re
import requests
import yaml

def is_valid_hostname(hostname):
    if len(hostname) > 255:
        return False
    if hostname[-1] == ".":
        hostname = hostname[:-1] # strip exactly one dot from the right, if present
    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in hostname.split("."))

peterlowe_list_url = 'https://pgl.yoyo.org/adservers/serverlist.php?hostformat=nohtml&showintro=0'
stevenblack_list_url = 'https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts'

q = requests.get(peterlowe_list_url)

# *.doubleclick.net	CNAME	.

header = '''---

'''

with open(r'/home/jason/fuckads/dns_block_list/vars/main.yml', 'w') as file:
    file.write(header)

hostlist = q.text.split('\n')

q = requests.get(stevenblack_list_url)

for host in q.text.split('\n'):
    if re.search(r'^0.*[a-zA-Z]',host):
        fqdn = host.split()[1]
        if is_valid_hostname(fqdn):
            hostlist.append(fqdn)

hostlist = list(filter(None, hostlist))
hostlist = list(set(hostlist))


data = {}
data['blacklist_hosts'] = hostlist

with open(r'/home/jason/fuckads/dns_block_list/vars/main.yml', 'a') as file:
    documents = yaml.dump(data, file, default_style='""')
